<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bookstore</title>
  <!-- add the styles -->
  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">
</head>
<body>
  @include('sections.header')
  <div class="flex-center position-ref full-height">
    <div class="container">
      <div class="content">
        @yield('content')
      </div>
    </div>
  </div>
  @include('sections.footer')
</body>
</html>
