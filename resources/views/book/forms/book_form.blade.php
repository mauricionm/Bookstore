<div class="form-group{{ ($errors->has('title')) ? $errors->first('title') : '' }}">
  {!! Form::label('title','Title:') !!}
  {!! Form::text('title',NULL,['class'=>'form-control','placeholder'=>'Insert a title']) !!}
  {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ ($errors->has('author')) ? $errors->first('author') : '' }}">
  {!! Form::label('author','Author:') !!}
  {!! Form::text('author',NULL,['class'=>'form-control','placeholder'=>'Insert a author']) !!}
  {!! $errors->first('author', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ ($errors->has('publisher')) ? $errors->first('publisher') : '' }}">
  {!! Form::label('publisher','Publisher:') !!}
  {!! Form::text('publisher',NULL,['class'=>'form-control','placeholder'=>'Insert a publisher']) !!}
  {!! $errors->first('publisher', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ ($errors->has('image')) ? $errors->first('image') : '' }}">
  {!! Form::label('image','Image:') !!}
  {!! Form::text('image',NULL,['class'=>'form-control','placeholder'=>'Insert a image']) !!}
  {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
  {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
</div>