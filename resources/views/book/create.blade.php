<!--extends from the master layout-->
@extends('layouts/master')

<!-- section content page-->
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h1>Create Book</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      {!! Form::open(['route'=>'book.store', 'method'=>'POST']) !!}
        @include('book.forms.book_form')
      {!! Form::close() !!}
    </div>
  </div>
@stop