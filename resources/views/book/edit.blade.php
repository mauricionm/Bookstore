
@extends('layouts/master')

<!-- section content page-->
@section('content')
  <div class="row">
    <div class="col-md-12">
      <h1>Edit Book</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      {!! Form::model($book,['route'=>['book.update', $book->id], 'method'=>'PUT']) !!}
         @include('book.forms.book_form')
      {!! Form::close() !!}
    </div>
  </div>
@stop