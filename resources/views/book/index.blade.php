<!--extends from the master layout-->
@extends('layouts/master')

<!-- section content page-->
@section('content')
  <div class="row">
    <div class="col-md-12">
      <a href="{{ route('book.create') }}" class="btn btn-info pull-right">Create New Book</a><br/><br/>
    </div>
  </div>
  <table class="table table-striped">
    <tr>
      <th>No.</th>
      <th>Title</th>
      <th>Author</th>
      <th>Publiched</th>
      <th>Image</th>
      <th>Created at</th>
      <th>Updated at</th>
      <th>Actions</th>
    </tr>
    <?php $index = 1; ?>
    @foreach($books as $book)
      <tr>
        <td>{{ $index++ }}</td>
        <td>{{ $book->title }}</td>
        <td>{{ $book->author }}</td>
        <td>{{ $book->publisher }}</td>
        <td>{{ $book->image }}</td>
        <td>{{ $book->created_at }}</td>
        <td>{{ $book->updated_at }}</td>
        <td>
          <form action="{{ route('book.destroy',$book->id) }}" method="post">
            <input type="hidden" name="_method" value="delete">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <a href="{{ route('book.edit', $book->id) }}" class="btn btn-primary">Edit</a>
            <input type="submit" class="btn btn-danger" value="Delete" onclick="return confirm('Are you sure?')">
          </form>
        </td>
      </tr>
    @endforeach
  </table>
@stop