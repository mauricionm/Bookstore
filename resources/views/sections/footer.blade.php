<div class="footer">
   <div class="container">
      <div class="row">
         <div class="col-md-4">
            <p>Book store Test</p>
            <p>2017</p>
         </div>

         <div class="col-md-4">
            <p>San Jose</p>
            <p>Costa Rica</p>
         </div>
         <div class="col-md-4">
            <p><strong>Contact us</strong></p>
            <p>bookstore@test.com</p>
            <p>+506 34231212</p>
         </div>
      </div>
   </div>
</div>