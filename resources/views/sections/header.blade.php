<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1>Book Store Test</h1>
        <img src="{{ asset('/img/book-icon.png') }}" width="100" height="100"/>
      </div>

      <div class="col-md-4">
        <a href="{{ url('/book') }}">Log in ></a>
        <br/>
        <a href="{{ url('/book') }}">Manage Books ></a>
      </div>
    </div>
  </div>
</div>