<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Book;

class BookController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    //show data
    $books = Book::all();
    return view('book.index',['books' => $books]);
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    //create view
    return view('book.create');
  }
  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    //validation
    $this->validate($request, [
      'title'=>'required',
      'author'=>'required',
      'publisher'=>'required',
      'image'=>'required',
    ]);
    //create new data
    $book = new book;
    $book->title = $request->title;
    $book->author = $request->author;
    $book->publisher = $request->publisher;
    $book->image = $request->image;
    $book->save();
    return redirect()->route('book.index')->with('alert-success','Data has been saved!');
  }
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $book = Book::findOrFail($id);
    //return to the view
    return view('book.edit', ['book'=> $book]);
  }
  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    //validation
    $this->validate($request, [
      'title'=>'required',
      'author'=>'required',
      'publisher'=>'required',
      'image'=>'required',
    ]);

    //create new data
    $book = Book::findOrFail($id);
    $book->title = $request->title;
    $book->author = $request->author;
    $book->publisher = $request->publisher;
    $book->image = $request->image;
    $book->save();
    return redirect()->route('book.index')->with('alert-success','Data has been saved!');
  }
  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $book = Book::findOrFail($id);
    $book->delete();
    return redirect()->route('book.index')->with('alert-success','Data has been deleted!');
  }
}
